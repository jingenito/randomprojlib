﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MathForm
{
    public partial class MathForm : Form
    {
        private bool IsLoaded;
        private Dictionary<FormLookup, string> combosource { get; set; }

        public MathForm()
        {
            InitializeComponent();
        }

        private void MathForm_Load(object sender, EventArgs e)
        {
            if (!IsLoaded && !DesignMode)
            {
                combosource = new Dictionary<FormLookup, string>();
                combosource.Add(FormLookup.PrimeFactorization, "Prime Factorization");
                combosource.Add(FormLookup.GCD_LCD, "GCD/LCD");
                cboFormSelecter.DataSource = new BindingSource(combosource, null);
                cboFormSelecter.DisplayMember = "Value";
                cboFormSelecter.ValueMember = "Key";
                IsLoaded = true;
            }
        }

        private void btnOpenForm_Click(object sender, EventArgs e)
        {
            Form frm;
            FormLookup key;
            if(Enum.TryParse(cboFormSelecter.SelectedValue.ToString(), out key))
            {
                switch (key)
                {
                    case FormLookup.PrimeFactorization:
                        this.Hide();
                        frm = new Forms.PrimeFactorization();
                        frm.FormClosed += FormClosedHandler;
                        frm.ShowDialog();
                        break;
                    case FormLookup.GCD_LCD:
                        this.Hide();
                        frm = new Forms.GCD_LCD();
                        frm.FormClosed += FormClosedHandler;
                        frm.ShowDialog();
                        break;
                }
            }
        }

        private void FormClosedHandler(object sender, EventArgs e)
        {
            this.Show();
        }
    }
}
