﻿public enum FormLookup
{
    MathForm = 0,
    PrimeFactorization = MathForm + 1,
    GCD_LCD = PrimeFactorization + 1
}