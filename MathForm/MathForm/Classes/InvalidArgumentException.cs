﻿namespace MathForm.Classes
{
    class InvalidArgumentException : System.Exception
    {
        public InvalidArgumentException()
        {

        }

        public InvalidArgumentException(string message) : base(message)
        {

        }
    }
}
