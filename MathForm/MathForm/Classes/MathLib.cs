﻿using System;
using System.Collections.Generic;

namespace MathForm.Classes
{
    public class MathLib
    {
        public static bool IsPrime(int number)
        {
            if(number < 2 || number >= int.MaxValue) { return false; }
            for(int i = 2; i < number; i++)
            {
                if(number % i == 0) { return false; }
            }
            return true;
        }

        private static int firstPrimeFactor(int number)
        {
            for(int i = 2; i < number; i++)
            {
                if(IsPrime(i) && number % i == 0)
                {
                    return i;
                }
            }
            return number;
        }
        
        public static List<int> PrimeFactors(int number)
        {
            var primes = new List<int>();
            while (!IsPrime(number))
            {
                var prime = firstPrimeFactor(number);
                primes.Add(prime);
                number = (int)Math.Floor((double)number / prime);
            }
            primes.Add(number); //while loop breaks when last int is prime
            return primes;
        }

        public static int GCD(int num1, int num2)
        {
            if(num1 < 1 || num2 < 1) { throw new InvalidArgumentException("Arguments must be Natural Numbers"); }
            if(num1 < num2)
            {
                //swaps the numbers if num2 > num1
                var temp = num1;
                num1 = num2;
                num2 = temp;
            }

            var remainder = 0;
            do {
                remainder = num1 % num2;
                num1 = num2;
                num2 = remainder;
            }while (num2 > 0) ;
            return num1;
        }

        public static int GCD(List<int> nums)
        {
            if(nums.Count < 2) { throw new InvalidArgumentException("Too Few Arguments"); }
            var gcd = GCD(nums[0], nums[1]);
            for(int x = 2; x < nums.Count; ++x)
            {
                gcd = GCD(gcd, nums[x]);
            }
            return gcd;
        }

        public static int LCM(int num1, int num2)
        {
            if (num1 <= 0 || num2 <= 0) { throw new InvalidArgumentException("Arguments must be Natural Numbers"); }
            return (num1 * num2) / GCD(num1, num2);
        }

        public static int LCM(List<int> nums)
        {
            if (nums.Count < 2) { throw new InvalidArgumentException("Too Few Arguments"); }
            var lcm = LCM(nums[0], nums[1]);
            for(int x = 2; x < nums.Count; ++x)
            {
                lcm = LCM(lcm, nums[x]);
            }
            return lcm;
        }
    }
}
