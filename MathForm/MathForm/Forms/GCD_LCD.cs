﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MathForm.Classes;

namespace MathForm.Forms
{
    public partial class GCD_LCD : Form
    {
        public GCD_LCD()
        {
            InitializeComponent();
        }

        private void GCD_LCD_Load(object sender, EventArgs e)
        {
            lblOutput.Text = "LCM:";
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            string[] input = txtInput.Text.Split(',');
            var nums = new List<int>();
            if(input.Length > 1)
            {
                foreach(string s in input)
                {
                    int n;
                    if(int.TryParse(s, out n))
                    {
                        nums.Add(n);
                    }
                    else
                    {
                        MessageBox.Show("One or more arguments is not a number");
                        return;
                    }
                }
            }
            else
            {
                MessageBox.Show("Too Few Arguments");
            }

            CalculateOutput(nums);
        }

        private void CalculateOutput(List<int> nums)
        {
            var output = 0;
            try
            {
                if (rbLCM.Checked)
                {
                    output = MathLib.LCM(nums);
                }
                else
                {
                    output = MathLib.GCD(nums);
                }
            }catch(InvalidArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            txtOutput.Text = output.ToString();
        }

        private void rbCheckedChangedHandler(object sender, EventArgs e)
        {
            var rb = (RadioButton)sender;
            lblOutput.Text = rb.Text + ":";
        }
    }
}
