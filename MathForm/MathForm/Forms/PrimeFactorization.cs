﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MathForm.Forms
{
    public partial class PrimeFactorization : Form
    {
        public PrimeFactorization()
        {
            InitializeComponent();
        }

        private void PrimeFactorization_Load(object sender, EventArgs e)
        {
            rbListView.Checked = true;
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            int number = validateInput(txtInput.Text);
            if(number == 0)
            {
                MessageBox.Show("Enter a valid input.");
                txtInput.Text = "";
            }
            else
            {
                var primes = Classes.MathLib.PrimeFactors(number);
                txtOutput.Text = (rbListView.Checked ? ListToString(primes) : ListToStringSimplified(primes));
            }
        }

        private int validateInput(string input)
        {
            int number;
            if(int.TryParse(input, out number))
            {
                if(number < 2 || number >= int.MaxValue) { return 0; }
                return number;
            }
            return 0;
        }

        private string ListToString(List<int> list)
        {
            var sb = new StringBuilder();
            sb.Append("[");
            foreach(int x in list)
            {
                sb.Append(x + ", ");
            }
            var str = sb.ToString();
            return str.Substring(0, str.Length - 2) + "]";
        }

        private string ListToStringSimplified(List<int> list)
        {
            if(list.Count == 1)
            {
                return string.Format("[{0}]", list[0]);
            }
            var sb = new StringBuilder();
            sb.Append("[");

            var iteration = 0; //loop iteration
            var counter = 1; //exponent count - should never be 0
            var prev = 0; //previous int - init to 0
            var last = list.LastIndexOf(list.Last()); //last index
            foreach(int x in list)
            {
                if(iteration == 0)
                {
                    prev = x;
                }
                else if(x == prev)
                {
                    counter++;
                    if (iteration == last)
                    {
                        sb.Append(prev + (counter == 1 ? ", " : " ^ " + counter + ", "));
                    }
                }
                else
                {
                    sb.Append(prev + (counter == 1 ? ", " : " ^ " + counter + ", "));
                    if (iteration == last)
                    {
                        sb.Append(x + ", ");
                    }
                    prev = x;
                    counter = 1;
                }
                iteration++;
            }
            var str = sb.ToString();
            return str.Substring(0, str.Length - 2) + "]";
        }
    }
}
