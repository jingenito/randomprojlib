﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace FileExecuter.Forms
{
    public partial class SetupForm : Form
    {
        public SetupForm()
        {
            InitializeComponent();
        }

        private void SetupForm_Load(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtDirectory.Text) && !string.IsNullOrWhiteSpace(txtPreset.Text))
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "XML-File | *.xml";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    var ds = CreateDS();
                    if(TryPrintXML(sfd.FileName, ds))
                    {
                        MessageBox.Show("Save Preset Successful!");
                    }
                }
            }
            else
            {
                MessageBox.Show("Directory and Preset Required");
            }
        }

        private DataSet CreateDS()
        {
            var ds = new DataSet();
            var dt = ds.Tables.Add();
            dt.TableName = txtPreset.Text.Trim();

            var arr = txtJAR.Text.Split(',');
            if (string.IsNullOrWhiteSpace(arr[0]))
            {
                AddColsToDT(ref dt, "Directory", "NumJARs");
                var dr = dt.Rows.Add();
                dr["Directory"] = txtDirectory.Text.Trim();
                dr["NumJARs"] = 0;
            }
            else
            {
                var cols = new List<string>();
                cols.Add("Directory"); cols.Add("NumJARs");
                for (int i = 0; i < arr.Length; ++i)
                {
                    if (!string.IsNullOrWhiteSpace(arr[i]))
                    {
                        cols.Add("JAR" + (i + 1));
                    }
                }
                AddColsToDT(ref dt, cols);
                var dr = dt.Rows.Add();

                dr["Directory"] = txtDirectory.Text.Trim();
                dr["NumJARs"] = arr.Length;
                for (int i = 0; i < arr.Length; ++i)
                {
                    dr["JAR" + (i + 1)] = arr[i];
                }
            }
            return ds;
        }

        private void AddColsToDT(ref DataTable dt, params string[] cols)
        {
            foreach(string col in cols)
            {
                dt.Columns.Add(col);
            }
        }

        private void AddColsToDT(ref DataTable dt, List<string> cols)
        {
            foreach(string col in cols)
            {
                dt.Columns.Add(col);
            }
        }

        private bool TryPrintXML(string FileName, DataSet ds)
        {
            try
            {
                ds.WriteXml(FileName);
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            return true;
        }
    }
}
