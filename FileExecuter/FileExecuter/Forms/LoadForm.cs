﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Text;
using System.Diagnostics;

namespace FileExecuter.Forms
{
    public partial class LoadForm : Form
    {
        public LoadForm()
        {
            InitializeComponent();
        }

        private void LoadForm_Load(object sender, EventArgs e)
        {

        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog cfd = new OpenFileDialog();
            cfd.Filter = "XML-File | *.xml";
            cfd.FilterIndex = 1;
            cfd.Multiselect = false;

            if(cfd.ShowDialog() == DialogResult.OK)
            {
                XMLToScreen(cfd.FileName);
            }
        }

        private void btnCompile_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(txtDirectory.Text) || !string.IsNullOrWhiteSpace(txtFile.Text))
            {
                var cmd = "";
                var output = GetCompileOutput(ref cmd);
                ExecuteCMD(cmd);
                txtOutput.Text = output;
            }
            else
            {
                MessageBox.Show("Directory and File Name required");
            }
        }

        private string GetCompileOutput(ref string cmd)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(txtDirectory.Text + Environment.NewLine);

            var arr = txtJAR.Text.Split(',');
            if (string.IsNullOrWhiteSpace(arr[0]))
            {
                cmd = "javac " + txtFile.Text + ".java";
                sb.Append(cmd + Environment.NewLine);
                sb.Append("java " + txtFile.Text);
            }
            else
            {
                string jars = "";
                for (int i = 0; i < arr.Length; ++i)
                {
                    jars += arr[i] + ", ";
                }
                jars = jars.Substring(0, jars.Length - 2);
                cmd = "javac -cp " + jars + " " + txtFile.Text + ".java";
                sb.Append(cmd + Environment.NewLine);
                sb.Append("java -cp " + jars + ".; " + txtFile.Text);
            }
            return sb.ToString();
        }

        private void ExecuteCMD(string cmd)
        {
            ProcessStartInfo startinfo = new ProcessStartInfo("cmd.exe");
            startinfo.WorkingDirectory = txtDirectory.Text;
            startinfo.UseShellExecute = false;
            startinfo.CreateNoWindow = true;
            startinfo.RedirectStandardInput = true;
            startinfo.RedirectStandardOutput = true;
            
            Process proc = new Process();
            proc.StartInfo = startinfo;
            proc.Start();

            proc.StandardInput.WriteLine(cmd);
            proc.WaitForExit();
            proc.Close();
        }

        private void XMLToScreen(string FileName)
        {
            var ds = new DataSet();
            try
            {
                ds.ReadXml(FileName);
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            if(ds.Tables.Count > 0)
            {
                var dt = ds.Tables[0];
                var dr = dt.Rows[0];
                txtDirectory.Text = dr["Directory"].ToString();
                var numJARs = 0;
                if(int.TryParse(dr["NumJARs"].ToString(), out numJARs))
                {
                    string jars = "";
                    if(numJARs > 0)
                    {
                        for(int i = 0; i < numJARs; ++i)
                        {
                            string colname = "JAR" + (i + 1);
                            jars += dr[colname].ToString() + ", ";
                        }
                        jars = jars.Substring(0, jars.Length - 2);
                    }
                    txtJAR.Text = jars;
                }
                else
                {
                    MessageBox.Show("Error Loading Preset");
                }
            }
        }
    }
}
