﻿using System;
using System.Windows.Forms;
using FileExecuter.Forms;

namespace FileExecuter
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            this.Hide();
            var frm = new LoadForm();
            frm.FormClosed += OnFormClosed;
            frm.ShowDialog();
        }

        private void btnSetup_Click(object sender, EventArgs e)
        {
            this.Hide();
            var frm = new SetupForm();
            frm.FormClosed += OnFormClosed;
            frm.ShowDialog();
        }

        private void OnFormClosed(object sender, EventArgs e)
        {
            this.Show();
        }
    }
}
