def isPrime(n):
    number = int(n)
    if number < 2 :
        return False
    elif number == 2:
        return True
    for x in range(2, number):
        if number % x == 0:
           return False
    return True

def firstPrimeFactor(n):
    number = int(n)
    for x in range(2, number):
        if isPrime(x) and number % x == 0:
            return x
    return number

def PrimeFactors(n):
    number = int(n)
    list = []
    while not isPrime(number):
        prime = firstPrimeFactor(number)
        list.append(prime)
        number //= prime
    list.append(number)
    return list

while True:
    number = input("Enter a number: ")
    print(PrimeFactors(number))



